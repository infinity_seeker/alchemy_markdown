defmodule AlchemyMarkdownTest do
  use ExUnit.Case
  doctest AlchemyMarkdown

  test "italicizes" do
  	str = "it *italicizes*"
    assert AlchemyMarkdown.tohtml(str) =~ "it <em>italicizes</em>" 
  end

  test "expand big tests" do
  	str = "some ++big++ text" 
  	assert AlchemyMarkdown.tohtml(str) =~ "<big>big</big> text" 
  end

  test "expand small tests" do
  	str = "some --small-- text" 
  	assert AlchemyMarkdown.tohtml(str) =~ "<small>small</small> text" 
  end

  test "expand hr tags" do
  	str1 = "stuff over the line\n---"
  	str2 = "stuff over the line\n***"
  	str3 = "stuff over the line\n- - -"
  	str4 = "stuff over the line\n*    *    *"
  	str5 = "stuff over the line*    *    *"

  	Enum.each([str1,str2,str3,str4], 
  		fn(str) -> assert AlchemyMarkdown.hrs(str) == "stuff over the line\n<hr />" end)
  	assert AlchemyMarkdown.hrs(str5) == str5
  end

end
